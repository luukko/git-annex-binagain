I created this package to use git-annex on Arch Linux without installing the
entire Haskell ecosystem. For someone who only needs git-annex, installing the
200+ Haskell packages, many of which get a pkgrel bump *every single day*,
seems like a large overhead.

The git-annex upstream kindly provides a standalone binary package. However,
this package takes the static approach to the extreme by bundling together
*everything*, including glibc. It is basically a kernel image short of a
virtual machine container. This package tries to provide a middle ground by
just using the git-annex binaries, which are dynamically linked against system
libraries, from the standalone package. At the time of this writing this seems
to work without any shimming or other trickery.

Please note that this package is provided solely on a "seems to work for me"
basis. The arch is currently set only to x86_64 since that is what I use.
Patches for including support for other architectures are welcome.
